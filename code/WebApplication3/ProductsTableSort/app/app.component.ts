﻿import { Component } from '@angular/core';

export class Item {
    name: string;

    count: number;

    constructor(name: string, count: number) {

        this.name = name;
        this.count = count;

    }
}

@Component({
    selector: 'purchase-app',
    template: `<div class="page-header">
        <h1> Test </h1>
    </div>
    <div class="panel">

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Count</th>

                </tr>
            </thead>
            <tbody>
                <tr *ngFor="let item of items">
                    <td>{{item.name}}</td>
                    <td>{{item.count}}</td>

                </tr>
            </tbody>
        </table>
    </div>`
})
export class AppComponent {
    items: Item[] =
    [
        { name: "Apple", count: 3 },
        { name: "Banana", count: 1 },
        { name: "Orange", count: 2 },
        { name: "Shugar", count: 1 },
        { name: "Dog", count: 1 }
    ];
 
}